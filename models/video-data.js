const mongoose = require('mongoose');

const videoDataSchema = new mongoose.Schema(
    {
        data: mongoose.SchemaTypes.Mixed,
        countOfVehicle: Number,  
        trafficFlow: Number,
        location: {},
        uploadedBy: mongoose.SchemaTypes.ObjectId,
    },
    {
        collection: 'video-data',
        timestamps: true,
    }
);

module.exports = mongoose.model('VideoData', videoDataSchema);
