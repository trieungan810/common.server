var express = require('express');
const mongoose = require('mongoose');
// var usersRouter = require('./routers/user.router');
var indexRouter = require('./routers/index');

const appConfig = require('./configs/app.config');
const cors = require('cors');

var app = express();
configExpress(app);

connectToDb();

module.exports = app;

function connectToDb() {
    mongoose.connect(appConfig.db.url, options = {
        user: appConfig.db.user,
        pass: appConfig.db.pass,
        useNewUrlParser: true
    }).then(() => {
        console.log('Connect to db successfully!');
    }).catch((err) => {
        console.log('Connect to db failed!');
    });
}

function configExpress(app) {
    app.use(express.json());
    app.use(cors({ origin: true, credentials: true }));
    app.use('/', indexRouter);
    // app.use('/users', usersRouter);

}