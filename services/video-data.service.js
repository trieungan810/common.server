const VideoData = require('../models/video-data');

class VideoDataService {
    async getAll() {

    }

    async saveData(data) {
        handleData(data);
        return await VideoData.create(data);
    }

    async getByUser(userId) {
        return await VideoData.find({uploadedBy: userId});  
    }
}

module.exports = new VideoDataService();

function handleData(data) {
    console.log(data);
}