const videoDataService = require('../services/video-data.service');

class VideoDataController {
    async getAll(req, res) {

    }

    async saveData(req, res) {
        await videoDataService.saveData(req.body);
        res.json(true);
    }

    async getByUser(req, res) {
        await videoDataService.getByUser(req.headers.userId);
    }
}

module.exports = new VideoDataController();
