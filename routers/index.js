const express = require('express');
const router = express.Router();
const authenMiddleware = require('../middlewares/authentication.middleware');
const videoDataController = require('../controllers/video-data.controller');

router.get('/video-data', authenMiddleware.verifyToken, videoDataController.getAll);
router.post('/video-data', videoDataController.saveData);

module.exports = router;