exports.verifyToken = (req, res, next) => {
    const token = req.body.token || req.params.token || req.headers.authorization;
    if (!token) {
        return next(new Error('AUTHENTICATION_FAILED'));
    }

    if (token != process.env.SERVER_API_KEY) {
        return next(new Error('AUTHENTICATION_FAILED'));
    }

    return next();
}

